/* https://nightwatchjs.org/api/commands/
I'm simulating TS by assigning default variables

# MAL website test
+ ava https://myanimelist.net/
+ kontrolli pealkirja
+ Leia top animed
+ + Airing
+ + Upcoming
+ + Popular
+ Vajuta disagree privacy policy aknal (janky)
+ tee screenshot

*/

const config = require('../../nightwatch.conf.js');

function SplitRegex(n = 4, splitter = '\n', modifier = 'g') {
  const pattern = Array(n).fill(`[^${splitter}]+`).join(splitter);
  return new RegExp(pattern, modifier);
}

class Rankings {
  digestTypes = ['airing', 'upcoming', 'popular'];

  constructor(WB) {
    this.WB = WB;
  }

  static GetAnimeInfo(value = '') {
    let rows = Rankings.RemoveUndesiredInfo(value);

    function ExpandRow(index, expansion) {
      const array = expansion(rows[index]);
      return [...rows.slice(0, index), ...array, ...rows.slice(index + 1)];
    }

    const check = ' members'; // Only ranking digest has members as last value
    if (value.includes(check, value.length - check.length - 2)) {
      // Gets rid of text from member count
      rows[3] = rows[3].split(' ')[0].toString();

      // Expands info relayed in the "type row" (where Type, Episode count, and Score is)
      rows = ExpandRow(2, (row) => row.replace(/ /g, '').split(','));
    } else {
      // Makes score be in the right place and removes user score
      [rows[3], rows[5]] = [rows[5], rows[3]]; // Switches score out with date
      rows[3] = rows[3].split(' ')[0].toString(); // Gets rid of user score

      // Seperates episodes from type
      rows = ExpandRow(2, (row) => row.replace(')', '').split(' ('));
    }

    const anime = {};
    [anime.rank,
      anime.name,
      anime.type,
      anime.episodes,
      anime.score,
      anime.members,
      anime.airingDate,
    ] = rows;
    return anime;
  }

  static RemoveUndesiredInfo(val = '') {
    const undesired = {
      rows: ['Manga Store ', 'Add to list', 'Watch Episode '],
      words: [' members', ' eps', ' scored '],
    };

    function RemoveRows(row = '') {
      for (let i = 0; i < undesired.rows.length; i++) {
        const undesiredRow = undesired.rows[i];
        if (row.startsWith(undesiredRow)) return '';
      }
      return row;
    }

    function ReplaceWords(row = '') {
      let output = row;
      undesired.words.forEach((x) => {
        output = output.replace(x, '');
      });
      return output;
    }

    let rows = val.split('\n');
    rows = rows.map((x) => {
      const edit = RemoveRows(x);
      if (edit === '') return edit;
      return ReplaceWords(edit);
    }).filter((x) => x);

    return rows;
  }

  TopAnimeAt(i) {
    this.WB.getText({
      selector: '.ranking-list',
      index: i,
    }, (element) => {
      if (element.status !== 1) {
        // eslint-disable-next-line no-console
        const anime = Rankings.GetAnimeInfo(element.value);
        // eslint-disable-next-line no-console
        console.log(`The anime at ${anime.rank}. place in the `
          + `"Top Anime" page is:\n${anime.name}\n`);
      }
    });
  }

  RankDigestAllAt(i) {
    this.digestTypes.forEach((type) => {
      const selector = `.${type}_ranking`;
      this.WB.waitForElementVisible(selector)
        .getText(selector, (result) => {
          const anime = Rankings.AnimeDigest(result, i);
          // eslint-disable-next-line no-console
          console.log(`The ${anime.rank}. ${type} anime is\n${anime.name}\n`);
        });
    });
  }

  static AnimeDigest(result, j = 0) {
    function RemoveQuickAddInfo(text) {
      // Removes Quick Add to user list (if logged in, conveys status)
      const splitter = '\n';

      // Start and end of quick add info
      const SoQA = text.indexOf(splitter) + splitter.length;
      const EoQA = text.indexOf(splitter, SoQA) + splitter.length;

      const quickAdd = text.slice(SoQA, EoQA);
      return text.replace(new RegExp(quickAdd, 'g'), '');
    }

    if (result.status === -1) throw RangeError('Empty rank digest');
    // Removes digest information
    const SoRI = result.value.indexOf('1'); // Start of rank info
    let anime = result.value.slice(SoRI); // Cuts the unnecessary part out
    anime = RemoveQuickAddInfo(anime);

    // Splits it into individual anime
    // Got this from https://stackoverflow.com/a/29197967
    anime = anime.match(SplitRegex()); // Splits after every fifth \n

    if (j === -1) return anime.map((x) => Rankings.GetAnimeInfo(x));
    const i = Rankings.RankCheck(j, anime.length);
    return Rankings.GetAnimeInfo(anime[i]);
  }

  static RankCheck(rank, max) {
    if (rank >= max) {
      // eslint-disable-next-line no-console
      console.error(`Desired rank (${rank + 1}) could not be reached\n`
        + `Showing the result at ${max}. place instead`);
      // eslint-disable-next-line no-param-reassign
      rank = max - 1;
    }
    return rank;
  }
}

const assertTitle = {
  homepage: 'MyAnimeList.net - Anime and Manga Database and Community',
  topAnime: 'Top Anime - MyAnimeList.net',
};
const consent = {
  id: 'qc-cmp2-ui',
  disagree: 'button[mode="secondary"]',
  agree: 'button[mode="primary"]',
};

module.exports = {
  'Anime search': function (browser) {
    browser // Load Site
      .windowSize('current', 1280, 720)
      .url('https://myanimelist.net/')
      .waitForElementVisible('body')
      .assert.title(assertTitle.homepage);

    // Get Top Anime from ranking digests
    const animeRanks = new Rankings(browser);
    animeRanks.RankDigestAllAt(0);

    // Clicks disagree on privacy policy screen
    browser.waitForElementPresent(`#${consent.id}`)
      .element('id', consent.id, (x) => {
        if (x.status !== -1) {
        // first button with this\/ style is Disagree
          browser.click(consent.disagree);
          browser.waitForElementNotPresent(`#${consent.id}`);
        }
      }).saveScreenshot(`${config.imgpath(browser)}MAL-Homepage.png`);

    // Getting top anime from "Top Anime" page
    browser.moveToElement('xpath', '//div[@id="menu_left"]/ul/li', 5, 5)
      .click('link text', 'Top Anime')
      .waitForElementVisible('body')
      .assert.title(assertTitle.topAnime)
      .saveScreenshot(`${config.imgpath(browser)}MAL-TopAnime.png`);
    animeRanks.TopAnimeAt(0);

    // Types something random in the search bar
    const searchValue = 'topSearchText';
    const randomString = Math.random().toString(36).substring(7); // https://stackoverflow.com/a/8084248
    browser.assert.elementPresent(`#${searchValue}`)
      .click(`#${searchValue}`)
      .setValue(`#${searchValue}`, randomString)
      .click('#topSearchButon'); // Not a typo

    browser.pause(2500).end();
  },
};
